/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanadon199.robotproject;

/**
 *
 * @author Kitty
 */
public class robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int n;
    private  char lastdirection = ' ' ;
    
    public robot(int x,int y,int bx, int by,int n){
        this.x=x;
        this.y=y;
        this.bx=bx;
        this.by=by;
        this.n=n;
    }
        public boolean inmap(int x ,int y){
            if(x>=n || x<0 || y>=n || y<0) {
                return false;
        }
                return true;
        }
    public boolean walk(char direction){
        switch(direction){
            case 'N':
                if(inmap(x,y-1)){
                    printUnablemove();
                    return false;
                }
                y = y -1;
                break;
            case 'S':
                if(inmap(x,y+1)){
                    printUnablemove();
                    return false;
                }
                y = y +1;
                break;
            case 'E':
                if(inmap(x+1,y)){
                    printUnablemove();
                    return false;
                }
                x = x +1;
                break;
            case 'W':
                if(inmap(x-1,y)){
                    printUnablemove();
                    return false;
                }
                x = x -1;
                break;
        }
        lastdirection = direction;
        if(isbomb()){
            printbomb();
        }
        return true;
    }
    public boolean walk(char direction,int step){
        for(int i=0; i<step; i++){
            if(!robot.this.walk(direction)){
                return false;
            }
        }
       return true;
    }
    public boolean walk(){
        return robot.this.walk(lastdirection);
    }
    public boolean walkNstep(int step){
        return robot.this.walk(lastdirection,step);
    }
    public String toString(){
        return "robot(" + this.x + " , "+this.y + ")";
    }
   
    public void printUnablemove(){
        System.out.print("I Cant Move");
    }
    public void printbomb(){
        System.out.print("Bomb found");
    }
    public boolean isbomb(){
        if(x == bx && y == by){
            return true;
        }
        return false;
    }
}
